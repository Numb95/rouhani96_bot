Requirements
============
you need `libjpeg-dev` and `zlib1g-dev` packages to be installed on your computer, before installing.

Install
=======
To make it work, do the following:

    $ git clone https://danialbehzadi@gitlab.com/danialbehzadi/rouhani96_bot.git
    $ cd rouhani96_bot
    $ virtualenv -p python3 --no-site-packages --distribute .env
    $ source .env/bin/activate
    $ pip3 install -r requirements.txt
    $ nohup ./run.sh &

